﻿using System;
using CollegeBuffer.DAL.Model;

namespace CollegeBuffer.BLL.Interfaces
{
    internal interface ICommentsRepository
    {
        Comment CreateComment(string text, User user, Guid parentEntityGuid);

        bool SafeDeleteComment(Comment comment);
    }
}
