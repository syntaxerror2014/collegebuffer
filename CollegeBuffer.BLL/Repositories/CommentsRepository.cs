﻿using System;
using System.Linq;
using CollegeBuffer.BLL.Interfaces;
using CollegeBuffer.DAL.Context;
using CollegeBuffer.DAL.Model;

namespace CollegeBuffer.BLL.Repositories
{
    public class CommentsRepository : BaseRepository<Comment>, ICommentsRepository
    {
        public CommentsRepository(DatabaseContext context)
            : base(context)
        {
        }

        public Comment CreateComment(string text, User user, Guid parentEntityGuid)
        {
            user = DbContext.Users.Find(user.Id);

            if (user == null)
                return null;

            var announcement = DbContext.Announcements.Find(parentEntityGuid);
            var ev = DbContext.Events.Find(parentEntityGuid);

            var comment = new Comment
            {
                Text = text,
                Date = DateTime.Now,
                User = user
            };

            if (announcement != null)
                comment.Announcement = announcement;

            else comment.Event = ev;

            return Insert(comment);
        }

        public bool SafeDeleteComment(Comment comment)
        {
            if (@comment.Replies.Count <= 0) return Delete(@comment);

            return @comment.Replies.All(SafeDeleteComment) && Delete(@comment);
        }
    }
}