﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CollegeBuffer.BLL;
using CollegeBuffer.DAL.Model;
using CollegeBuffer.Models;
using CollegeBuffer.Special;

namespace CollegeBuffer.Controllers
{
    public class CommentsController : Controller
    {
        //
        // GET: /Announcements/
        public string New(string text, string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();
            if (MySession.Current.UserDetails == null)
                return "F";

            var db = DbUnitOfWork.NewInstance();
            var myUser = db.UsersRepository.Get(MySession.Current.UserDetails.Id);
            
            return db.CommentsRepository.CreateComment(text, myUser, new Guid(id)) != null ? "K" : "F";
        }

       
	}
}